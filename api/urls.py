from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = [
    path('drinks/', views.drink_list, name='drink-list'),
    path('drinks/<str:pk>', views.drink_detail, name='drink-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
